# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Our teams repository is for Website development and collaboration ###

* Throughout the duration of the semester teams will be randomly formed to execute development for both websites and mobile applications. By using Bitbucket the team's efficiency to work cohesively will increase and make collaboration easier 

* The version of the Bootstrap template is 3.2.7


### How do I get set up? ###

To get the repository fully set up review tutorial write-up provided in Blackboard 

### Contribution ###

* Compiling members: Cristian Zamarripa
* Readme write-up: Cristian Zamarripa 
* Tutorial write-up: Cristian Zamarripa
* Git commands tutorial: Trevor Dye 
* Bootstrap template executor: Yoni Moore, James Walker
* Writing tests: Trevor Dye, Yoni Moore, Courtenay Cronin
* Making edits: everyone in group 3

### Who do I talk to? ###
If you have any questions or concerns please feel free to contact us directly (850) 282-3341 or through email Webdevgroup3@gmail.com

*Owner and creator of this repository: Cristian Zamarripa
*Administrative team: Courtenay Cronin, Trevor Dye,  James Walker, and Yoni Moore